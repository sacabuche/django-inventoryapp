# coding=utf-8
from django import template
from inventory.models import Transaction, ProductTransaction

register = template.Library()

@register.filter
def transaction_status(value):
    _status = dict(Transaction.STATUS_CODES)
    return _status.get(value, '')

@register.filter
def transaction_nature(value):
    _status = dict(Transaction.NATURE_CODES)
    return _status.get(value, '')

@register.filter
def product_transaction_status(value):
    _status = dict(ProductTransaction.STATUS_CODES)
    return _status.get(value, '')


