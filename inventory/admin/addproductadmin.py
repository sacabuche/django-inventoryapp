#-*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from django.utils.translation import ugettext_lazy as _

from inventory.models import (Product, ProductModel, Price,
                                           ExtraDescription,
                                           ProductModelQuantity, ProductImage,
                                           Brand, Location, Transaction,
                                           ProductTransaction, ProductEntry,
                                           ProductExit)


class ProductModelInline(admin.TabularInline):
    model = ProductModel
    extra = 0


class ProductAdmin(ModelAdmin):
    inlines = (ProductModelInline, )

admin.site.register(Product, ProductAdmin)
admin.site.register(ProductModel)
admin.site.register(Price)
admin.site.register(ExtraDescription)
admin.site.register(ProductModelQuantity)
admin.site.register(ProductImage)
admin.site.register(Brand)
admin.site.register(Location)
admin.site.register(Transaction)
admin.site.register(ProductTransaction)
admin.site.register(ProductEntry)
admin.site.register(ProductExit)
