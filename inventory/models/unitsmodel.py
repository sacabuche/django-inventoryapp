#-*- coding: utf-8 -*-
from django.db import models

class Unit(models.Model):
    name = models.CharField('nombre', max_length=40, unique=True)
    symbol = models.CharField('simbolo', max_length=20, unique=True)

    class Meta:
        app_label = 'inventory'

    def __unicode__(self):
        return '%s (%s)'%(self.name, self.symbol)

    # FIXME: Monkey patch for eventsmanager
    # Dont use is depracated
    @property
    def nombre(self):
        return self.name

    # FIXME: same of self.name
    @property
    def simbolo(self):
        return self.symbol

    @classmethod
    def get(cls, unit, return_none=False):
        """
        Get the object or return none
        """
        if unit is None:
            if return_none is not False:
                return return_none
            raise ValueError

        if isinstance(unit, cls):
            return unit

        try:
            unit = int(unit)
        except (ValueError, TypeError):
            pass

        # search by symbol
        if isinstance(unit, (str, unicode)):
            try:
                return cls.objects.get(symbol=unit)
            except cls.DoesNotExist:
                if return_none is not False:
                    return return_none
                raise

        #search by id
        elif isinstance(unit, int):
            try:
                return cls.objects.get(id=unit)
            except cls.DoesNotExist:
                if return_none is not False:
                    return return_none

        # if not object raise Error
        raise TypeError("unit_or_symbol, must be an <Unit>, <str>\
                        or <int> object")

