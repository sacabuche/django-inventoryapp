#-*- coding: utf-8 -*-
from django.db import models

class Contact(models.Model):
    name = models.CharField('nombre', max_length=15, unique=True)
    address_line1 = models.CharField('dirección', max_length=64, blank=True)
    address_line2 = models.CharField('dirección', max_length=64, blank=True)
    address_line3 = models.CharField('dirección', max_length=64, blank=True)
    address_line4 = models.CharField('dirección', max_length=64, blank=True)
    phone_number1 = models.CharField('numero telefónico', max_length=32,
                                     blank=True)
    phone_number1 = models.CharField('numero telefónico', max_length=32,
                                     blank=True)
    notes = models.TextField('notas', blank=True)

    class Meta:
        app_label = 'inventory'
        abstract = True

class Supplier(Contact):
    pass

class Buyer(Contact):
    pass

