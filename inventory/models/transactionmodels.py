#-*- coding: utf-8 -*-
from django.db import models, transaction
from django.db.models import Sum
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe

from polymorphic.manager import PolymorphicManager
from polymorphic.polymorphic_model import PolymorphicModel
from unitsmodel import Unit
from decimal import *



class TransactionManager(models.Manager):

    def new_entry(self, *args, **kwargs):
        return self.start_transaction(nature = Transaction.ENTRY, *args, **kwargs)

    def new_exit(self, *args, **kwargs):
        return self.start_transaction(nature = Transaction.EXIT, *args, **kwargs)

    def new_movement(self, *args, **kwargs):
        return self.start_transaction(nature = Transaction.MOVE, *args, **kwargs)

    def start_transaction(self, location, nature, comments=''):
        t = Transaction(location = location,
                   comments = comments,
                   nature = nature,
                   status = Transaction.PROCESSING
                   )
        t.save()
        return t



class Transaction(models.Model):

    # STATUS CODES
    PROCESSING = 0
    REGISTERED = 1

    STATUS_CODES = (
        (PROCESSING, 'procesando'),
        (REGISTERED, 'registrada'),
    )

    # NATURES
    ENTRY = 0
    EXIT = 1
    MOVE = 2
    #ADJUST = 3

    NATURE_CODES = (
        (ENTRY, 'entrada'),
        (EXIT, 'salida'),
        (MOVE, 'movimiento'),
     #   (ADJUST, 'ajuste'),
    )


    date_time = models.DateTimeField(auto_now_add=True)
    location =  models.ForeignKey('Location', verbose_name='lugar')
    comments = models.TextField('Comentarios', blank=True)
    nature = models.SmallIntegerField('tipo', choices=NATURE_CODES)
    status = models.SmallIntegerField('estado', choices=STATUS_CODES,
                                      default=PROCESSING)
    products = models.ManyToManyField('ProductTransaction', blank=True,
                                      null=True)

    objects = TransactionManager()

    change_status = True

    class Meta:
        app_label = 'inventory'
        ordering = ('-date_time',)

    def __unicode__(self):
        return "%s - %s"%(self.nature, self.date_time)

    def __get_pt_class(self, nature):
        """
        return a ProductTransaction class
        """
        PT = {
            self.ENTRY: ProductEntry,
            self.EXIT: ProductExit,
            self.MOVE: ProductMove,
        }
        return PT[nature]

    def get_pt_class(self):
        return self.__get_pt_class(self.nature)

    @models.permalink
    def get_absolute_url(self):
        return ('inv_transaction_detail', [self.id])

    def get_total_cost(self):
        total_cost = sum((product.get_cost() for product in self.products.all()))
        return total_cost

    def add_product(self, *args, **kwargs):
        """
        Add Product based in the nature of transaction
        """
        if self.status == self.REGISTERED:
            raise Exception("Transaction registered, no more adds")
        p_transaction = self.get_pt_class()(*args, **kwargs)
        p_transaction.save()
        self.products.add(p_transaction)
        return p_transaction

    def remove_product(self, product=None, product_id=None):
        try:
            product.id
        except AttributeError:
            product = self.products.get(id=product_id)
        self.products.remove(product)
        product.delete()

    def cancel(self):
        if self.status == self.REGISTERED:
            return

        for p_transaction in self.products.all():
            p_transaction.delete()

        self.delete()

    def end(self, force=False):
        """
        Register all the products and can't be ENDED TWICE
        and change the final status

        If all the products were registeres will return REGISTERED,
        if not will return PROCESSING

        """
        if self.status == self.REGISTERED:
            return

        self.change_status = True
        for p_transaction in self.products.all():
            try:
                p_transaction.end(force)
            except ProductTransaction.RegisterException:
                # cant be marked as REGISTERED
                # until next try
                self.change_status = False

        # Set new status
        if self.change_status:
            self.status = self.REGISTERED
            self.save()

        return self.status

    def force_end(self):
        self.end(force=True)

    @models.permalink
    def get_absolute_url(self):
        return ('inv_transaction_detail', [str(self.id)])


class ProductTransaction(PolymorphicModel):

    PROCESSING = 0
    REGISTERED = 1

    STATUS_CODES = (
        (PROCESSING, 'procesando'),
        (REGISTERED, 'registrado'),
    )

    STATUS_CODES_DICT = dict(STATUS_CODES)

    product = models.ForeignKey('ProductModel')
    quantity = models.DecimalField('cantidad', max_digits=45, decimal_places=20)
    total_cost = models.DecimalField('costo total', max_digits=45,
                                     decimal_places=20, default=Decimal(0),
                                     help_text='llenar este o "costo por unidad"'
                                    )
    unit_cost =  models.DecimalField('costo por unidad', max_digits=45,
                                     decimal_places=20, default=Decimal(0),
                                     help_text='llenar este o "costo total"'
                                    )
    status = models.SmallIntegerField('estado', choices=STATUS_CODES,
                                      editable=False, default=PROCESSING)

    __transaction = None


    exclude_fields = set(('id', 'polymorphic_ctype', 'producttransaction_ptr'))
    field_filters = {'quantity':{'floatformat':(2,)},
                     'total_cost':{'floatformat':(2,)},
                     'unit_cost':{'floatformat':(2,)},
                     # status codes filter is in self.__init__
                     }

    class Meta:
        app_label = 'inventory'
        ordering = ('product__product__name',)

    class RegisterException(Exception):
        pass

    def __init__(self, *args, **kwargs):
        super(ProductTransaction, self).__init__(*args, **kwargs)
        # Add a status filter #FIXME very hackish
        self.field_filters.update({'status':{self.get_code_text: tuple()}})

    def __unicode__(self):
        return '%s-%s'%(self.product.sku, self.quantity)

    def get_code_text(self, code):
        return self.STATUS_CODES_DICT[code]

    @models.permalink
    def delete_url(self):
        return ('inv_delete_t_product', [self.id])

    def get_transaction(self):
        return self.transaction_set.all()[0]

    def as_table_headers(self):
        fields = self.get_fields()
        data = list()
        data.append('<tr>')
        for field_name, field in fields.iteritems():
            data.append('<th>%s</th>'%field.verbose_name.decode('utf-8'))
        data.append('</tr>')
        data = u''.join(data)
        return mark_safe(data)


    def as_table(self):
        """
        Return this product wrapped in <tr><td>
        """
        fields = self.get_fields()
        #fields.pop(0) #delete id

        data = list()
        # add headers
        data.append('<tr>')
        for field_name, field in fields.iteritems():
            if isinstance(field, models.ForeignKey):
                value = getattr(self, field.name)
            else:
                value = field.value_to_string(self)
            name = field.verbose_name
            info = "<td>%s</td>"%(value)

            data.append(info)
        data.append('</tr>')
        data = u''.join(data)
        data = data.replace('\n', '<br/>')
        return mark_safe(data)

    def get_transaction(self):
        if not self.__transaction:
            self.__transaction = self.transaction_set.all()[0]
        return self.__transaction

    def get_location(self):
        return self.get_transaction().location

    def clean(self):
        """
        Verify if unit_cost and total_cost are not set at the same time
        """
        if self.unit_cost and self.total_cost:
            raise ValidationError("El costo solo puede ser definido en uno de\
                                  los dos campos (por 'costo x unidad' o 'costo total')")

    def _register(self, force):
        """
        This is the method have to be written for every TansctionProduct
        make the register process
        """
        raise NotImplementedError

    def _postregister(self, force):
        """
        set the postprocessing status
        """
        self.status = self.REGISTERED
        self.save()

    def _pre_register(self, force):
        """
        Some validations
        """
        if force:
            return
        if self.quantity is Decimal(0):
            raise self.RegisterException('quantity is 0, nothing to register')

    def end(self, force=False):
        """
        Register the transaction, but if an error happen on _register
        w'ont execute _post_register(status change)

        If this is ENDED ONCE, can't be ended twice

        The validation occurs in the next top level, which will continue
        to register other TransactionProducts if this is called from
        Transaction.end()

        The exceptito that is raised cames from:
            self.RegisterExeption
        """
        if self.status is self.REGISTERED:
            return

        self._pre_register(force)
        self._register(force)
        self._postregister(force)

    def get_cost(self):
        """
        Returns the unit cost
        """
        #this can go in other function
        if self.unit_cost != Decimal(0):
            return self.unit_cost
        else:
            return self.total_cost / self.quantity

    @classmethod
    def get_fields(self):
        return (field
                for field in self._meta.fields
                if field.name not in self.exclude_fields)

    def get_values(self):
        """
        Iter values and aplyes some filters or functions
        setted on self.field_filters,
        The filters are imported from django.template.defaultfilters
        """
        from django.template import defaultfilters

        for field in self.get_fields():
            value = getattr(self, field.name)

            # return the value when there are no filter
            if field.name not in self.field_filters:
                yield str(value)
            # apply filter or function
            else:
                filter_content = self.field_filters[field.name]
                for filter_name, attrs in filter_content.iteritems():
                    # filter is a function
                    if hasattr(filter_name, '__call__'):
                        _filter = filter_name

                    # import the function dinamicly
                    # XXX: May be import first all the necesary filters ??
                    elif isinstance(filter_name, str):
                        _filter = getattr(defaultfilters, filter_name)
                    else:
                        raise ValueError('must be a string or a function')
                    # Apply de filter with their arguments
                    value = _filter(value, *attrs)
                yield value

    @classmethod
    def get_fields_name(self):
        for field in self.get_fields():
            yield field.verbose_name


class ProductEntry(ProductTransaction):
    expiry_date = models.DateField('caducidad', blank=True, null=True)
    description = models.TextField('descripción', blank=True)
    supplier = models.ForeignKey('Supplier', verbose_name='proveedor', null=True, blank=True)

    exclude_fields = set(('description',))
    field_filters = dict(expiry_date={'default':('---',)},
                         supplier={'default':('---',)})

    class Meta:
        app_label = 'inventory'

    def __init__(self, *args, **kwargs):
        super(ProductEntry, self).__init__(*args, **kwargs)
        self.__update_custom_attributes()

    def __update_custom_attributes(self):
        update_attrs = ('exclude_fields', 'field_filters')
        for attr in update_attrs:
            # Get the base attribute
            attribute = getattr(self, attr, tuple())
            # update the set with the parent class attribute
            parent_values = getattr(super(ProductEntry, self), attr, None)
            attribute.update(parent_values)
            # set to self again the attribute
            setattr(self, attr, attribute)

    @transaction.commit_on_success
    def _register(self, force=False):
        cost = self.get_cost()
        product = self.product

        #TODO: Add a extra description asocieted to this Class
        product.add_description(content=self.description)
        product.add_supplier(self.supplier)
        product.add_expiry_date(self.expiry_date)
        product.add_products(self.get_location(), self.quantity, cost)


class ProductExit(ProductTransaction):
    price = models.DecimalField('precio', max_digits=45, decimal_places=20,
                               default=Decimal(0))
    buyer = models.ForeignKey('Buyer', verbose_name='comprador',
                              null=True, blank=True)
    quantity_before = models.DecimalField('cantidad', max_digits=45,
                                          decimal_places=20, editable=False,
                                          null=True)

    class Meta:
        app_label = 'inventory'

    def _pre_register(self, force=False):
        """
        validate if quantity is lower than current quantity of product
        """
        super(ProductExit, self)._pre_register(force)
        if force:
            return
        if self.quantity > self.product.get_quantity(self.get_location()):
            raise self.RegisterException('Not enough products to checkout')

    def _register(self, force=False):
        product = self.product
        location = self.get_location()

        # set values for hsitoric
        self.quantity_before = product.get_quantity(location)
        self.unit_cost = product.get_cost(location)
        self.save()

        product.remove_products(location, self.quantity, force)
        product.add_buyer(self.buyer)


class ProductMove(ProductTransaction):
    destination = models.ForeignKey('Location', related_name='destination')
    notes = models.TextField('descripción', blank=True)

    class Meta:
        app_label = 'inventory'

    def _register(self):
        self.product.register_move(self)


class ProductOpen(ProductTransaction):
    #this will be saved after the operation
    subproduct = models.ForeignKey('ProductModel',
                                   related_name='p_open_transactions',
                                   null=True, blank=True)
    subquantity = models.DecimalField('cantidad contenida', max_digits=45,
                                           decimal_places=20)
    new_units = models.ForeignKey('Unit', null=True, blank=True,
                                  verbose_name='unidades del contenido')

    class Meta:
        app_label = 'inventory'

    def _register(self):
        self.product.register_open(self)
