#-*- coding: utf-8 -*-
from django.db import models
from polymorphic.manager import PolymorphicManager
from polymorphic.polymorphic_model import PolymorphicModel

from taggit.managers import TaggableManager
from decimal import *

from unitsmodel import Unit


class Product(PolymorphicModel):
    """
    The base of Inventory
    """
    ARCHIVED = 0
    ACTIVE = 1

    STATUS_CODES = (
        (ARCHIVED, 'archivado'),
        (ACTIVE, 'activo')
    )

    sku = models.CharField('clave', max_length=15, unique=True)
    name = models.CharField('nombre', max_length=60)
    brand = models.ForeignKey('Brand', verbose_name='marca',
                              null=True, blank=True)
    description = models.TextField('descripción', blank=True)
    tax = models.DecimalField('impuesto%', max_digits=5, decimal_places=2,
                              help_text='En %% ej:15.34', default=Decimal(0))
    status = models.SmallIntegerField('estado', choices=STATUS_CODES,
                                      default=ACTIVE)

    prices = models.ManyToManyField('Price', null=True, blank=True)
    #imagenes = models.ManyToManyField('ProductImage', null=True, blank=True)

    # supplier = SupliersManager
    # buyer = buyer Manager


    ##alerts
    #min quantity
    #max quantity

    tags = TaggableManager()
    date_time = models.DateField(auto_now_add=True)

    location = None

    class Meta:
        app_label = 'inventory'
        ordering = ('name',)

    def get_or_create_model(self, units, inner_units=None, inner_quantity=None):
        units= Unit.get(units)
        in_units = Unit.get(inner_units, return_none=None)
        q = self.models

        # set values for the object that Does not Exists
        defaults = {'units': units,
                    'contained_units': in_units,
                    'contained_quantity': inner_quantity}

        model, created = q.get_or_create(units = units,
                                         contained_units = inner_units,
                                         contained_quantity = inner_quantity,
                                         defaults = defaults)
        return model

    def needed_and_quantity(self, units, quantity, location=None):
        """
        search for the model in this units at location,
        >>> ingredient.location = location # this is posible too
        >>> has_all, q_to_buy = ingredient.needed_and_quantity(units, quantity)
        """

        # search for the model
        loc = self.__get_location(location)
        symbol = self.__get_units_symbol(units)
        try:
            model = self.models.get(units__symbol=symbol)
        except ProductModel.DoesNotExist:
            return (False, Decimal(quantity))

        needed = Decimal(quantity) - model.get_quantity(loc)

        # if has all
        if needed <= 0:
            return (True, Decimal(0))

        # just has a part or nothing
        return (False, needed)

    def __get_location(self, location=None):
        if self.location:
            return self.location
        elif location:
            return location
        else:
            raise Exception('location is needed')


    def __get_units_symbol(self, units):
        """
        return the text of units symbol
        """
        if isinstance(units, Unit):
            return units.symbol
        else:
            return units

    def get_cost(self, units, location=None):
        loc = self.__get_location(location)
        symbol = self.__get_units_symbol(units)
        try:
            model = self.models.get(units__symbol=symbol)
        except ProductModel.DoesNotExist:
            return Decimal(0)
        return model.get_cost(location)


class ExtraDescription(models.Model):
    content = models.TextField('contenido')

    class Meta:
        app_label = 'inventory'

class ExpiryDate(models.Model):
    date =  models.DateField('Caducidad')

    class Meta:
        app_label = 'inventory'

class Price(models.Model):
    value = models.DecimalField(max_digits=45, decimal_places=20)
    perefence_level = models.SmallIntegerField('nivel de preferencia',
                                               default=0,
                                               help_text='El más bajo es el\
                                               preferido')
    class Meta:
        app_label = 'inventory'

class ProductModel(models.Model):
    product = models.ForeignKey('Product', related_name='models')
    sku = models.CharField('clave', max_length=15)
    name = models.CharField('nombre', max_length=50, blank=True)
    description = models.TextField('descripción', blank=True)
    extra_descriptions = models.ManyToManyField('ExtraDescription', blank=True,
                                         null=True)
    prices = models.ManyToManyField('Price', null=True, blank=True)
    units = models.ForeignKey('Unit', null=True, blank=True,
                                   help_text='caja')
    contained_units = models.ForeignKey('Unit', null=True, blank=True,
                                       related_name = 'sub_units',
                                       help_text='botella, si el producto\
                                        contiene botellas, ml, si el producto\
                                        contine mililitros')
    contained_quantity = models.DecimalField(max_digits=20, decimal_places=6,
                                            null=True, blank=True,
                                             help_text='12, para una caja que\
                                             contenga 12 botellas')
    suppliers = models.ManyToManyField('Supplier', null=True, blank=True)
    buyers = models.ManyToManyField('Buyer', null=True, blank=True)
    expiry_dates = models.ManyToManyField('ExpiryDate', null=True, blank=True)

    class Meta:
        ordering = ('name',)
        unique_together = (('product', 'sku'),)
        app_label = 'inventory'

    class NoQuantityError(Exception):
        pass

    def __unicode__(self):
        product = self.product.name or ''
        units = self.units.symbol if self.units else ''
        contained = ''
        if self.contained_units:
            contained_units = self.contained_units.symbol
            contained = 'con %s (%s)'%(self.contained_quantity, contained_units)
        return '%s (%s) %s [%s]'%(product, units, contained, self.sku)

    def __set_auto_sku(self):
        get_id = lambda x: x and str(x.id) or '_'
        #get_symbol = lambda x: x and x.symbol or '_'

        id = get_id(self.product)
        units = get_id(self.units)
        c_units = get_id(self.contained_units)
        if self.contained_quantity:
            c_quantity = str(round(self.contained_quantity, 2))
        else:
            c_quantity = '_'
        # Build the sku and insert it
        self.sku = '-'.join((id, units, c_units, c_quantity))

    def save(self, *args, **kwargs):
        if not self.sku:
            self.__set_auto_sku()
        super(ProductModel, self).save(*args, **kwargs)

    def get_quantity_obj(self, location):
        q, created = self.quantities.get_or_create(location=location,
                            defaults={'location':location})
        return q

    def get_quantity(self, location):
        return self.get_quantity_obj(location).quantity

    def get_cost(self, location):
        return self.get_quantity_obj(location).cost

    def get_total_quantity(self):
        quantities = self.quantities.all()
        quantity = sum((q.quantity for q in quantities))
        return quantity or Decimal(0)

    def add_supplier(self, supplier):
        if supplier:
            self.suppliers.add(supplier)

    def add_buyer(self, buyer):
        if buyer:
            self.buyers.add(buyer)

    def add_expiry_date(self, date):
        if date:
            self.expiry_dates.create(date=date)

    def add_description(self, content='', obj=None):
        """
        Add an ExtraDescription if validation pass
        """
        if obj:
            self.extra_descriptions.add(obj)
        elif content:
            self.extra_descriptions.create(content=content)
        else:
            #nothing to do
            pass

    def add_products(self, location, quantity, cost):
        """
        Add a quantity to some location of this productModel
        """
        #get or create ProductModelQuantity
        q = self.get_quantity_obj(location)
        q.add(quantity, cost)

    def remove_products(self, location, quantity, force=False):
        """
        Remove products from this location
        """
        # get quantity for this location
        q = self.get_quantity_obj(location)
        #substract quantity
        q.substract(quantity, force)

    def register_move(self, move):
        pass
        # get quantity object or rise error
        # substract quantity from origin
        # add quantity to destiny
        # recaulculete cost on destiny

    def register_open(self, open_product):
        pass
        # get quantity object or rise error
        # substract quantity from product
        # get new quantity of open products
        # get subproduct model or create it
        # get quantity of subproduct
        # add new quntantity to subproduct
        # recalculate cost


class ProductModelQuantity(models.Model):
    model = models.ForeignKey('ProductModel', related_name='quantities')
    location = models.ForeignKey('Location')
    quantity = models.DecimalField(max_digits=45, decimal_places=20,
                                   default=Decimal(0))
    #the cost is per unit
    cost  = models.DecimalField(max_digits=45, decimal_places=20,
                               default=Decimal(0))

    class Meta:
        app_label = 'inventory'

    class UnderflowError(Exception):
        pass

    def __unicode__(self):
        return ', '.join((self.location.name, str(self.quantity), str(self.cost)))

    def add(self, quantity, cost):
        """
        Add quantity of products,
        the cost is Weighted mean
        """
        #(q1*c1 + q2*c2 + ... +  qn*cn) / (q1 + q2 + ... +qn)

        # initial values
        ZERO = Decimal(0)
        q1 = self.quantity if self.quantity > ZERO else ZERO
        c1 = self.cost
        #values to add
        q2 = quantity
        c2 = cost

        # q1*c1
        q1xc1 = q1 * c1
        q2xc2 = q2 * c2

        sum_qixci = q1xc1 + q2xc2
        sum_qi = q1 + q2

        if sum_qi > Decimal(0.001):
            new_cost = sum_qixci / sum_qi
        else:
            new_cost = cost

        #set the new values
        self.quantity += quantity
        self.cost = new_cost
        self.save()
        return self.cost

    def substract(self, quantity, force=False):
        """
        substract the quantity to products
        force allow negative numbers
        """
        if not force:
            if self.quantity < quantity:
                raise self.UnderflowError('Not enough products to substract,\
    %s - %s '%(self.quantity, quantity))

        self.quantity -= quantity
        self.save()
        return self.quantity



class ProductImage(models.Model):
    file = models.FileField('archvo', upload_to='products/images/%Y/%m')
    description = models.CharField('descripción', max_length=150)

    class Meta:
        app_label = 'inventory'

class Brand(models.Model):
    name = models.CharField('nombre', max_length=15, unique=True)
    #tags = models.

    class Meta:
        app_label = 'inventory'


class Location(models.Model):
    name = models.CharField('nombre', max_length=15, unique=True)
    address_line1 = models.CharField('dirección', max_length=64, blank=True)
    address_line2 = models.CharField('dirección', max_length=64, blank=True)
    address_line3 = models.CharField('dirección', max_length=64, blank=True)
    address_line4 = models.CharField('dirección', max_length=64, blank=True)
    phone_number1 = models.CharField('numero telefónico', max_length=32,
                                     blank=True)
    phone_number1 = models.CharField('numero telefónico', max_length=32,
                                     blank=True)
    notes = models.TextField('notas', blank=True)

    class Meta:
        app_label = 'inventory'

    def __unicode__(self):
        return self.name

