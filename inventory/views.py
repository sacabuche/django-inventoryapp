from django.views.generic import TemplateView, ListView
from django.shortcuts import redirect, get_object_or_404
from django.views.generic.base import View, TemplateResponseMixin
from django.views.generic.base import TemplateView
from django.core.urlresolvers import reverse

from inventory.forms.transactions import TransactionForm, get_product_form
from inventory.forms.products import ProductForm, ProductModelForm
from inventory.models import (Transaction, Location, Product,
                              ProductTransaction)


class PanelView(TemplateResponseMixin, View):
    template_name = 'inventory/panel.html'

    def get(self, request):
        t_o = Transaction.objects
        t_active = t_o.filter(status=Transaction.PROCESSING)
        t_registered = t_o.filter(status=Transaction.REGISTERED)[0:10]
        return self.render_to_response({'transactions':t_active,
                                        'transactions_registered': t_registered})

class DeleteTPView(View):
    """
    Deletes Product Transaction, No render
    """
    def get(self, request, id):
        pt = get_object_or_404(ProductTransaction, id=id,
                               status=ProductTransaction.PROCESSING)
        transaction = pt.get_transaction()
        pt.delete()
        return redirect(transaction)


class NewTransactionView(TemplateResponseMixin, View):
    template_name = 'inventory/new_transaction.html'
    nature = Transaction.ENTRY

    def get(self, request):
        try:
            location = Location.objects.all()[0]
        except IndexError:
            raise Exception('No Location setted')

        nature = self.nature
        to = Transaction.objects
        if nature == Transaction.ENTRY:
            t = to.new_entry(location=location)
        elif nature == Transaction.EXIT:
            t = to.new_exit(location=location)
        elif nature == Transaction.MOVE:
            t = to.new_move(location=location)

        #form = TransactionForm(instance=t)
        #return self.render_to_response({'form':form})

        return redirect(t)


class TransactionDetailView(TemplateResponseMixin, View):

    template_name = 'inventory/transaction_detail.html'

    def get_context(self, id):
        t = get_object_or_404(Transaction, id=id)
        product_fields = t.get_pt_class().get_fields_name()
        tform = TransactionForm(instance=t)
        pt_form = get_product_form(transaction=t)
        product_form = ProductForm()
        product_model_form = ProductModelForm()
        products = t.products.all()
        all_products = Product.objects.filter(status=Product.ACTIVE)

        return {'transaction':t,
                'transaction_form': tform,
                'products': products,
                'pt_form': pt_form,
                'all_products': all_products,
                'product_fields':product_fields,
                'product_form': product_form,
                'product_model_form': product_model_form,
               }

    def get(self, request, id):
        return self.render_to_response(self.get_context(id))

    def post(self, request, id):
        t = get_object_or_404(Transaction, id=id)
        # end transaction: save the changes
        POST = request.POST
        if POST.has_key('end'):
            # TODO: Take comments when ending
            t.end()
            return self.get(request, t.id)

        elif POST.has_key('cancel'):
            t.cancel()
            return redirect('inv_panel')

        tform = TransactionForm(request.POST)

        # saves transaction edits
        if tform.is_valid():
            tform.save()
        product_form = get_product_form(t, request.POST)
        if product_form.is_valid():
            # ProductTransaction
            p_t = product_form.save()
            t.products.add(p_t)

            # create empty form
            product_form = get_product_form(t)

        context = self.get_context(id)
        context['product_form'] = product_form
        return self.render_to_response(context)


class ListProductsView(ListView):
    context_object_name = "product_list"
    queryset = Product.objects.all()
    template_name = "inventory/product_list.html"

# generic view
def add_edit_product(request, pk=None):
    pass


# Add a model in some fashion
def add_edit_model(request, product_id, pk=None):
    pass

