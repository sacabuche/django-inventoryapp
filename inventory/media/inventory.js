(function( $ ) {
	$.widget( "ui.combobox", {
		options: {
			select: function( event, ui ) {
						ui.item.option.selected = true;
						self._trigger( "selected", event, {
							item: ui.item.option
						});
				},

			change: function( event, ui ) {
				if ( !ui.item ) {
					var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
						valid = false;
					select.children( "option" ).each(function() {
						if ( $( this ).text().match( matcher ) ) {
							this.selected = valid = true;
							return false;
						}
					});
					if ( !valid ) {
						// remove invalid value, as it didn't match anything
						$( this ).val( "" );
						select.val( "" );
						input.data( "autocomplete" ).term = "";
						return false;
					}
				}
			}

			},

		_create: function() {
			var args = arguments | {};
			var self = this,
				select = this.element.hide(),
				selected = select.children( ":selected" ),
				value = selected.val() ? selected.text() : "";
			var input = this.input = $( "<input>" )
				.insertAfter( select )
				.val( value )
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: function( request, response ) {
						var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
						response( select.children( "option" ).map(function() {
							var text = $( this ).text();
							if ( this.value && ( !request.term || matcher.test(text) ) )
								return {
									label: text.replace(
										new RegExp(
											"(?![^&;]+;)(?!<[^<>]*)(" +
											$.ui.autocomplete.escapeRegex(request.term) +
											")(?![^<>]*>)(?![^&;]+;)", "gi"
										), "<strong>$1</strong>" ),
									value: text,
									option: this
								};
						}) );
					},
					select: this.options.select,
					change: this.options.change

				})
				.addClass( "ui-widget ui-widget-content ui-corner-left" );

			input.data( "autocomplete" )._renderItem = function( ul, item ) {
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + item.label + "</a>" )
					.appendTo( ul );
			};

			this.button = $( "<button type='button'>&nbsp;</button>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Show All Items" )
				.insertAfter( input )
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "ui-corner-right ui-button-icon" )
				.click(function() {
					// close if already visible
					if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
						input.autocomplete( "close" );
						return;
					}

					// work around a bug (likely same cause as #5265)
					$( this ).blur();

					// pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
					input.focus();
				});
		},

		destroy: function() {
			this.input.remove();
			this.button.remove();
			this.element.show();
			$.Widget.prototype.destroy.call( this );
		}
	});
})( jQuery );

/*resolve deeper attributes and is silent fault*/
function get_attr(obj, attr){
	var undef;
	attr = attr.split('.');

	while (obj && attr[0]){
		obj = obj[attr.shift()]  || undef;
	}

	return obj
}

function clear_models(){
	$('#models-to-select .product-model').remove();
}

function set_models(data){
	hide_forms();
	$('#product-models').show(200);
	var table = $('#models-to-select');
	clear_models();
	var dom_table = table[0];
	var FIELDS = ['sku', 'units.symbol', 'contained_units.symbol', 'contained_quantity'], field, field_name;
        for (var i=0; i < data.length; i++){
		var tr = dom_table.insertRow(i+1);
		$(tr).addClass('product-model');
		$(tr).attr('data-id', data[i].id);
		for (var field_i=0; field_i < FIELDS.length; field_i++){
			var td = tr.insertCell(field_i);
			field_name = FIELDS[field_i];
			field = get_attr(data[i], field_name);
			$(td).html(field);
		}
	}
}

function on_select_model(event){
	hide_forms();
	$('#product-form-wrapper').show(200);
	$('#product-models').show(200);
	var id = $(event.target).closest('tr').addClass('selected').attr('data-id');
	$('#id_product')[0].value = id;
}


/*load the product models via ajax*/
function load_product_models(product_id){

	function set_api_url(){
	var url;
	/*save a copy of the original url*/
	    if (! inventory_api.__models){
		inventory_api.__models = inventory_api.models
	    }
	    url = inventory_api.__models.replace('0', product_id);
	    inventory_api.models = url;
	}

	set_api_url()
	$('#p-model-form-wrapper input[name=product]')[0].value = product_id;
	$.get(inventory_api.models, set_models);
}

function on_select_product(event, ui){
	var product_id = ui.item.option.value;
	load_product_models(product_id);	
}

function display_product_error(jqXHR, textStatus, errorThrown){
}
var display_errors = display_product_error;

function clear_autocomplete(){
  $('#add-product-buttons input')[0].value = '';
}

function add_to_product_list(data, clone){
	var form = $('#save').attr('disabled', "false").closest('form');

	$('#product-form-wrapper').replaceWith(clone.clone());
	$('.datepicker').datepicker();
	clear_models();
	clear_autocomplete();
	hide_forms();

	var table = $('#products-added')[0];
	$('.new-product').removeClass('new-product');
	var tr = table.insertRow(1);
	$(tr).addClass('new-product');
	var fields = data.values;
	for(var i=0; i < fields.length; i++){
		var td = tr.insertCell(i);
		$(td).html(fields[i]);
	}
	/*add delete url*/
	var td = tr.insertCell(fields.length);
	var anchor = document.createElement('a');
	anchor.href = data.delete_url;
	$(anchor).html("Borrar");
	td.appendChild(anchor);
}


function hide_forms(){
	$('.product-model').removeClass('selected');
	$('.form').hide(100);
}

function open_model_form(event){
	event.preventDefault();
	hide_forms();
	$('#product-models').show(200);
	$('#p-model-form-wrapper').show(200);
}

function add_created_product(data, clone){
	$('#p-form-wrapper').replaceWith(clone.clone());
	hide_forms();
	var option = $(document.createElement('option'));
	option.attr('value', data.id);
	option.html(data.name);
	option.prependTo($('#products'));
	var input = $('#add-product-buttons .ui-autocomplete-input');
	input[0].value = data.name;
	load_product_models(data.id);
}

/* This function make an ajax call, and srerialize the thata of one form
 * the values neded to pass like an object are:
 *  event
 *  url
 *  success
 * */
function create_object(obj){
	var e = obj.event,
	    url = obj.url,
	    success = obj.success,
	    clone = obj.event.data.clone;
	
	e.preventDefault();

	var data = $(e.target).closest('form').serialize();
	$.ajax({
		type: 'POST',
		data: data,
		url: url,
		success: function (data){
			success(data, clone);
		},
		error: display_errors,
		dataType: 'json'
	});
}

function create_product(event){
	var obj = {
		'event':event,
		'url': inventory_api.products,
		'success': add_created_product
	}
	create_object(obj);
}

function reload_models(data, clone){
	$('#p-model-form-wrapper').replaceWith(clone.clone());
	load_product_models(data.product.id);
}

function create_model(event){
      var obj = {
	      'event': event,
	      'url': inventory_api.models,
	      'success': reload_models,
      }
      create_object(obj);
}

function create_transaction_product(e){
	var obj = {
		'event':e,
		'url': inventory_api.transaction_products,
		'success': add_to_product_list
	};
	/*Disable the button to send*/
	$('#save').attr('disabled', "true");
	create_object(obj);
}

function open_product_form(event){
	event.preventDefault();
	hide_forms();
	$('#p-form-wrapper').show(200);
}

function set_click_events(){
		/*openers*/
		$( ".product-model" ).live('click', on_select_model);
		$("#a-add-product-base").click(open_product_form);
		$("#a-create-model").click(open_model_form);

		/*saving click events*/
		var product_wrapper_clone =  $("#product-form-wrapper").clone();
		product_wrapper_clone.find('.hasDatepicker').removeClass('hasDatepicker');
		var transaction_obj = {"clone":product_wrapper_clone};
		$("#save").live('click', transaction_obj, create_transaction_product);
		
		var product_obj = {"clone":$('#p-form-wrapper').clone()};
		$("#save-product").live('click', product_obj, create_product);

		var model_obj =  {"clone":$('#p-model-form-wrapper').clone()};
		$("#save-product-model").live('click', model_obj, create_model);
}

$(function() {
		$( "#products" ).combobox({select:on_select_product});
		hide_forms();
		set_click_events();
	});
