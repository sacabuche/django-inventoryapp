from django.test import TestCase
from decimal import *

from models import (Product, ProductModel, ProductModelQuantity, Brand,
                    Location, ProductImage, Supplier, Buyer, Transaction,
                    ProductEntry, ProductExit, ProductMove, ProductOpen,
                    Unit, ProductTransaction)

from recipes.models import Ingredient


class InventoryTest(TestCase):
    def setUp(self):
        self.location1 = Location.objects.create(name='location1',
                                                address_line1='direccion uno',
                                                phone_number1='(1, 3332432',
                                                notes='notas acerca de esto'
                                               )
        self.location2 = Location.objects.create(name='location2',
                                                address_line1='direccion uno',
                                                phone_number1='(1, 3332432',
                                                notes='notas acerca de esto'
                                               )
        self.product = Ingredient.objects.create(sku='act',
                                             name='aceite',
                                             description='aceite de cocina',
                                             )
        self.unit_ml = Unit.objects.create(name='mililiter', symbol='ml')
        self.unit_lt = Unit.objects.create(name='liter', symbol='lt')
        self.unit_bottle = Unit.objects.create(name='bottle', symbol='btl')
        self.unit_box = Unit.objects.create(name='box', symbol='box')

        # adding models
        product = self.product
        model_bx = product.models.create(sku='bx',
                                       name='box',
                                       units = self.unit_box,
                                       contained_units = self.unit_bottle,
                                       contained_quantity = Decimal(12),
                                      )
        self.model_bx = model_bx
        model_btl = product.models.create(sku='btl',
                                          name='bottle',
                                          units = self.unit_bottle,
                                          contained_units = self.unit_ml,
                                          contained_quantity = Decimal(1000)
                                         )
        self.model_bottle = model_btl
        self.model_x = product.models.create()

    def test_verify_empty_model(self):
        """
        Test creating a model from the base product empty
        """
        self.assertEqual(self.model_x.id, 3)

    def test_get_or_create_model(self):
        product = self.product
        model = product.get_or_create_model(self.unit_lt)
        self.assertEqual('%s-%s'%(product.id, self.unit_lt.id), model.sku)

        model = product.get_or_create_model(self.unit_lt.id)
        self.assertEqual('%s-%s'%(product.id, self.unit_lt.id), model.sku)


    def test_transaction_entry(self):
        loc = self.location1
        t = Transaction.objects.new_entry(location=loc)
        cost = Decimal(20)
        pt1 = t.add_product(product=self.model_bx, quantity=Decimal(10),
                      unit_cost=cost)
        pt2 = t.add_product(product=self.model_bottle, quantity=Decimal(10),
                          total_cost=cost)

        # testing the Cost
        self.assertEqual(pt1.get_cost(), cost)
        self.assertEqual(pt2.get_cost(), cost/10)
        self.assertEqual(self.model_bx.get_cost(loc), Decimal(0))

        t.end()

        for pt in t.products.all():
            self.assertEqual(pt.status, ProductTransaction.REGISTERED)

        self.assertEqual(t.status, Transaction.REGISTERED)
        # first part of the test
        self.assertEqual(self.model_bx.get_cost(loc), cost)
        self.assertEqual(self.model_bottle.get_cost(loc), cost/10)

        # Transaction 2
        cost2 = Decimal(30)
        tt = Transaction.objects.new_entry(location=loc)
        tt.add_product(product=self.model_bx, quantity=Decimal(2),
                      unit_cost=cost2)
        tt.add_product(product=self.model_bottle, quantity=Decimal(3),
                          total_cost=cost2)

        self.assertEqual(tt.status, Transaction.PROCESSING)
        #end transaction
        self.assertEqual(tt.end(), Transaction.REGISTERED)

        for pt in tt.products.all():
            self.assertEqual(pt.status, ProductTransaction.REGISTERED)

        # second part
        self.assertEqual(
            self.model_bx.quantities.filter(location=self.location1).count(),
            1)
        self.assertAlmostEqual(self.model_bx.get_cost(loc),
                              (10*cost + 2*cost2)/(12), places=6)
        self.assertAlmostEqual(self.model_bottle.get_cost(loc),
                               (cost + cost2)/(13),
                               places=6
                              )

    def test_transaction_exit(self):
        loc = self.location1
        t = Transaction.objects.new_entry(location=loc)
        cost = Decimal(20)
        quantity = Decimal(10)
        pt1 = t.add_product(product=self.model_bx, quantity=quantity,
                      unit_cost=cost)
        pt2 = t.add_product(product=self.model_bottle, quantity=quantity,
                          total_cost=cost)
        t.end()

        t = Transaction.objects.new_exit(location=loc)
        t.add_product(product=self.model_bx, quantity=Decimal(1))
        t.add_product(product=self.model_bottle, quantity=quantity)


        self.assertEqual(t.end(), Transaction.REGISTERED)
        self.assertAlmostEqual(self.model_bx.get_quantity(loc),
                               quantity-1, places=6)
        self.assertAlmostEqual(self.model_bottle.get_quantity(loc),
                               Decimal(0), places=6)





