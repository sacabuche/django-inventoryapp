from django.conf.urls.defaults import *
from inventory.models import Transaction
from inventory.views import (PanelView, NewTransactionView,
                             TransactionDetailView, ListProductsView,
                             DeleteTPView)


urlpatterns = patterns('inventory.views',

    url(r'^$', PanelView.as_view(), name='inv_panel'),
    # Start transactions
    url(r'^transactions/start-entry/$',
        NewTransactionView.as_view(nature=Transaction.ENTRY),
        name='inv_start_entry'),

    url(r'^transactions/start-exit/$',
        NewTransactionView.as_view(nature=Transaction.EXIT),
        name='inv_start_exit'),

    url(r'^transactions/start-move/$',
        NewTransactionView.as_view(nature=Transaction.MOVE),
        name='inv_start_move'),

    url(r'^transactions/product-(?P<id>\d+)/delete/$',
        DeleteTPView.as_view(),
        name='inv_delete_t_product'),

    # End start transactions

    url(r'^transactions/(?P<id>\d+)/$',
        TransactionDetailView.as_view(),
        name='inv_transaction_detail'),

    url(r'^products/$',
        ListProductsView.as_view(),
        name='inv_product_list'),
)
