#coding=utf-8
from django import forms

class FilteredSelect(forms.widgets.Select):

    """
        Displays as an ordinary selectbox with an additional text-input for filtering the options.
        Requires: http://www.barelyfitz.com/projects/filterlist/filterlist.js

        Author: Konrad Giæver Beiske
        Company: Linpro
    """
    class Media:
        js = ('js/filterlist.js', )

    pre_html = """
                    <span>
                    Buscar: <input name="regexp" class="regexp"/>
                    <br/>
               """

    post_html = """
                    </span>
                """
    def render(self, name, value, attrs=None, choices=()):
        _name = name.replace('-','__')
        _class = attrs.get('class', '')
        attrs['class'] = _class + ' filtered-select'
        super_res = super(FilteredSelect, self).render(name, value, attrs=attrs, choices=choices)
        return self.pre_html + super_res + self.post_html
