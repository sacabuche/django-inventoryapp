from django.forms import ModelForm
from django.forms.models import modelformset_factory
from django import forms

from inventory.widgets import FilteredSelect
from inventory.models import (ProductModel, Product)


class ProductForm(ModelForm):
    class Meta:
        model = Product
        exclude = ('status', 'prices', 'tags', 'data_time')

class ProductModelForm(ModelForm):
    class Meta:
        widgets = {'product':forms.HiddenInput}
        model = ProductModel
        exclude = ('sku', 'extra_descriptions', 'prices', 'suppliers', 'buyers',
                  'expiry_dates')

