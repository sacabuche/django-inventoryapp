from django.forms import ModelForm
from django.forms.models import modelformset_factory
from django import forms

from inventory.widgets import FilteredSelect
from inventory.models import (Transaction, ProductEntry, ProductExit,
                              ProductMove, ProductTransaction, Product, Unit)

class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        exclude = ('products', 'status', 'nature', 'comments')

class ProductTransactionForm(ModelForm):
    class Meta:
        model = ProductTransaction
        exclude = ('status', 'product')


class ProductEntryForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProductEntryForm, self).__init__(*args, **kwargs)
        self.fields['expiry_date'].widget.attrs = {'class':'datepicker'}

    class Meta:
        widgets = {'product':forms.HiddenInput}
        model = ProductEntry

class ProductExitForm(ModelForm):
    class Meta:
        model = ProductExit
        widgets = {'product':forms.HiddenInput}
        exclude = ('status',)

class ProductMoveForm(ModelForm):
    class Meta:
        model = ProductMove
        widgets = {'product':forms.HiddenInput}
        exclude = ('status',)


MODELS = {Transaction.ENTRY: ProductEntryForm,
         Transaction.EXIT: ProductExitForm,
         Transaction.MOVE: ProductMoveForm}


def get_product_form(transaction, *args, **kwargs):
    form = MODELS[transaction.nature](*args, **kwargs)
    return form


