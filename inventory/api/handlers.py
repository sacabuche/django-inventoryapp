#from django.core.exceptions import ObjectDoesNotExist
#from django.http import HttpResponse
#from django.utils import simplejson
#from django.core.urlresolvers import reverse

from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import rc, require_mime, FormValidationError

from inventory.forms.transactions import get_product_form
from inventory.forms.products import ProductForm, ProductModelForm
from inventory.api.resource import validate
from inventory.models import (Product, ProductModel, Unit, Transaction,
                              ProductTransaction, Location)




class ProductHandler(BaseHandler):
    allowed_methos = ('GET', 'POST')
    model = Product
    exclude = ('_state', 'polymorphic_ctype_id',)

    @validate(ProductForm)
    def create(self, request):
        return request.form.save()

class ProductModelHandler(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = ProductModel
    exclude = ('_state',)

    @validate(ProductModelForm)
    def create(self, request, product__id):
        return request.form.save()


class UnitsHandler(BaseHandler):
    allowed_methods = ('GET',)
    model = Unit
    exclude = ('_state',)

class TransactionHandler(BaseHandler):
    allowed_methods = ('GET',)
    model = Transaction
    exclude = ('_state',)

class LocationHandler(BaseHandler):
    allowed_methods = ('GET',)
    model = Location
    exclude = ('_state',)

class ProductTransactionHandler(BaseHandler):
    allowed_methods = ('GET', 'POST')
    model = ProductTransaction
    fields = ('values', 'delete_url')
    exclude = ('_state',)

    @staticmethod
    def values(pt):
        return list(pt.get_values())

    def create(self, request, transaction__pk):
        try:
            transaction = Transaction.objects.get(pk=transaction__pk)
        except:
            return rc.NOT_FOUND
        form = get_product_form(transaction, request.data)
        if form.is_valid():
            product = form.save()
            transaction.products.add(product)
            return product #list(product.get_values())
        else:
            raise FormValidationError(form)
