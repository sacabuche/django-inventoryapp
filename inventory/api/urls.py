from django.conf.urls.defaults import *
from django.conf import settings

from piston.authentication import HttpBasicAuthentication
from piston.doc import documentation_view

from inventory.api.resource import Resource
from inventory.api.handlers import (ProductHandler, ProductModelHandler,
                                    TransactionHandler, ProductTransactionHandler)

product = Resource(handler=ProductHandler)
product_model = Resource(handler=ProductModelHandler)
transaction = Resource(handler=TransactionHandler)
product_transaction = Resource(handler=ProductTransactionHandler)

urlpatterns = patterns('',
        url(r'^products/$', product, name='api-inv-products'),
        url(r'^products/(?P<id>\d+)/$', product, name='api-inv-product'),
        url(r'^products/(?P<product__id>\d+)/models/$', product_model,
           name='api-inv-product-models'),
        url(r'^transactions/$', transaction,
            name='api-inv-transactions'),
        url(r'^transactions/(?P<pk>\d+)/$', transaction,
            name='api-inv-transaction'),
        url(r'^transactions/(?P<transaction__pk>\d+)/products/$',
            product_transaction,
            name='api-inv-transaction-products'),
)
