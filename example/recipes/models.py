from django.db import models
from inventory.models import Product

class Ingredient(Product):

    def __unicode__(self):
        return self.name

